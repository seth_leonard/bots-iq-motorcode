#include <PS3BT.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
USB Usb;

BTD Btd(&Usb); // set as USB device > Usb.Task();

PS3BT PS3(&Btd); // set as USB device > Usb.Task();

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Create the motor shield object with the default I2C address

Adafruit_DCMotor *mainmotor = AFMS.getMotor(1); // Set Object > Port M1

boolean printTemperature; // Data Type > Print the Temperature

boolean printAngle; // Data Type > Print the Angle


void setup() {
  Serial.begin(115200); // Serial Band
  pinMode(48, OUTPUT); // Set 48 > Output
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while(1); //halt
  }
  Serial.print(F("\r\nPS3 Bluetooth Library Started"));
  AFMS.begin();
  mainmotor->setSpeed(255); 
}


void loop() { // start loop

  Usb.Task(); // poll the connected USB devices


 if(PS3.getButtonClick(L1)) // Get Button Click of L1 Button 
        Serial.print(F("\r\nAttemtping... Motor M1 Run")); 
        mainmotor->run(FORWARD); // Run Motor @ 100 %

 if(PS3.getButtonClick(L2)) // Get Button Click of L2 Button
        Serial.print(F("\r\nAttemtping... Motor M1 Shutoff"));
        mainmotor->run(RELEASE); // Immediately shut off connection to Motor 1

if(PS3.getButtonClick(SQUARE)) // Get Button Click of PS3 SQUARE Button
        Serial.print(F("\r\nAttemtping... LED turnon")); 
        digitalWrite(48, HIGH); // Turn LED One 13, Ground         

if(PS3.getButtonClick(TRIANGLE)) //  Get Button Click for PS3 Triangle Button 
        Serial.print(F("\r\nAttemtping... LED shutoff")); 
        digitalWrite(48, LOW);  // Turn LED Off 13, Ground
// end loop
}
