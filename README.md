This repo is just used for maintaining code changes for our robotics project at UHS


### How To Setup ###

* Download Arduino IDE
* Download USB_Host_Shielf_Libary.zip from this repo
* Open Arduino IDE & Add Libary
![Screenshot at 16-10-10.png](https://bitbucket.org/repo/georka/images/699776529-Screenshot%20at%2016-10-10.png)
* Close Arduino IDE & Re-open
* Download SetupBluetooth.ino
* Open SetupBluetooth.ino
* Verify/Compile
* Upload Code to your board
* Once uploaded, reset and open serial monitor
* Make sure USB bluetooth adapter is plugged in
* Let the script do its thing and when it returns to the serial monitor the bluetooth address hot swap the USB for the PS3 controller using a USB connection to the controller
* Let it set the address to the controller & then unplug the controller and re-plug in the USB bluetooth adapter 
* You're now ready to control the Arduino using the PS3 controller.



### License ###

The MIT License (MIT)

Copyright (c) 2014 Seth Aikens

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.